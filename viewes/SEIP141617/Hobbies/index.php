<head>
    <title>Hobbies</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="style.css" type="text/css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>

<?php

require_once("../../../vendor/autoload.php");
use App\Hobbies\Hobbies;
use App\Message\Message;


$objHobbies = new Hobbies();

$allData = $objHobbies->index();
$serial = 1;
echo "<table  bgcolor='#00FF00' class='table-class' border='5px'>";

echo "<th> Serial </th>";
echo "<th> ID </th>";
echo "<th> Name </th>";
echo "<th> Hobbies </th>";
echo "<th> Action </th>";


foreach($allData as $oneData){
    echo "<tr style='height: 40px'>";
    echo "<td>".$serial."</td>";

    echo "<td>".$oneData->id."</td>";
    echo "<td>".$oneData->name."</td>";
    echo "<td>".$oneData->hobbies."</td>";


    echo "<td>";

    echo "<a href='view.php?id=$oneData->id'><button class='btn btn-info'>View</button></a> ";
    echo "<a href='edit.php?id=$oneData->id'><button class='btn btn-primary'>Edit</button></a> ";
    echo "<a href='trash.php?id=$oneData->id'><button class='btn btn-success'>Trash</button></a> ";
    echo "<a href='delete.php?id=$oneData->id'><button class='btn btn-danger'>Delete</button></a> ";


    echo "</td>";

    echo "</tr>";

    $serial++;
}

echo "</table>";
