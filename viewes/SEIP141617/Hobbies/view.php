<?php

require_once("../../../vendor/autoload.php");

use App\Hobbies\Hobbies;

$objHobbies = new Hobbies();
$objHobbies->setData($_GET);
$oneData = $objHobbies->view();

echo $oneData->id;
echo $oneData->name;
echo $oneData->hobbies;