<?php

require_once("../../../vendor/autoload.php");
use App\Message\Message;
use App\Hobbies\Hobbies;
if(!isset($_SESSION)) session_start();
echo Message::message();

$objHobbies = new Hobbies();
$objHobbies->setData($_GET);
$oneData = $objHobbies->view();

$hobbies_array = explode(",", $oneData->hobbies);
?>

<html>
<head>
    <title>Hobbies</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="style.css" type="text/css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>

<body>

<div class="container">
    <div class="row vertical-offset-100">
        <div class="col-md-4 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row-fluid user-row ">
                        <div class="book_title"><center><h2>Add Hobby</h2></center></div>
                        <div class="img_icon"><img src="../../../Resources/Images/hobbies.jpg" class="img-responsive" width="150px" height="150px" alt="Conxole Admin"/></div>
                    </div>
                </div>
                <div class="panel-body">
                    <form action="update.php" method="post" accept-charset="UTF-8" role="form" class="form-signin">

                        <fieldset>
                            <label class="panel-login">
                                <div class="login_result"></div>

                            </label>

                            <input type="hidden" name="id" value="<?php echo $oneData->id ?>">
                            <label>User Name : </label>
                            <input class="form-control" name="name" value="<?php echo $oneData->name; ?>"  type="text">
                            <br>
                            <label>Select Your Hobbies:</label>
                            <br>
                            <input  type="checkbox" name="hobbies[]" value="swimming" <?php if(in_array("swimming",$hobbies_array)) : ?> checked <?php endif; ?> />swimming<br />
                            <input  type="checkbox" name="hobbies[]" value="reading" <?php if(in_array("reading",$hobbies_array)) : ?> checked <?php endif; ?> />reading<br />
                            <input type="checkbox" name="hobbies[]" value="gaming" <?php if(in_array("gaming",$hobbies_array)) : ?> checked <?php endif; ?> />gaming<br />
                            <input  type="checkbox" name="hobbies[]" value="running" <?php if(in_array("running",$hobbies_array)) : ?> checked <?php endif; ?> />running<br />
                            <input  type="checkbox" name="hobbies[]" value="dancing" <?php if(in_array("dancing",$hobbies_array)) : ?> checked <?php endif; ?> />dancing</br>


                            <br>
                            <input class="btn btn-lg btn-success " type="submit" value="Add">
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function(){
            $(function() {
                $('#confirmation_message').delay(5000).fadeOut();
            });

        });
    </script>

</body>

</html>