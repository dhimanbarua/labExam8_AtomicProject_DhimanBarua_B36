<?php
require_once('../../../vendor/autoload.php');


use App\ProfilePicture\ProfilePicture;

$obProPic= new ProfilePicture();
$filename = $_FILES['profilePicture']['tmp_name'];

$located_folder = '../../../Resources/Profile_Picture/';

$uploaded_file = time() . $_FILES['profilePicture']['name'];
$destination = $located_folder . $uploaded_file;
move_uploaded_file($filename, $destination);
$_POST['profilePicture']=$uploaded_file;

$obProPic->setData($_POST);
$obProPic->store();
