<?php
require_once ('../../../vendor/autoload.php');
use App\ProfilePicture\ProfilePicture;

$objprofilePicture = new ProfilePicture();
$objprofilePicture->setData($_GET);
$objprofilePicture->delete();