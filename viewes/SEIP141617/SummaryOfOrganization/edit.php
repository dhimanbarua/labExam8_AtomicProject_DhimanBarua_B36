<?php

require_once("../../../vendor/autoload.php");
use App\Message\Message;
use App\SummaryOfOrganization\SummaryOfOrganization;

if(!isset($_SESSION)) session_start();
echo Message::message();
$objSummaryOfOrganization = new SummaryOfOrganization();
$objSummaryOfOrganization->setData($_GET);
$oneData = $objSummaryOfOrganization->view();

?>
<html>
<head>
    <title>Summary Of Organizatione</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="style.css" type="text/css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <div class="row vertical-offset-100">
        <div class="col-md-4 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row-fluid user-row ">
                        <div class="book_title"><center><h2>Edit Summary Of Organization</h2></center></div>
                        <div class="img_icon"><img src="../../../Resources/Images/org.jpg" class="img-responsive" width="150px" height="150px" alt="Conxole Admin"/></div>
                    </div>
                </div>
                <div class="panel-body">
                    <form action="update.php" method="post" accept-charset="UTF-8" role="form" class="form-signin">
                        <fieldset>
                            <label class="panel-login">
                                <div class="login_result"></div>
                            </label>

                            <input type="hidden" name="id" value="<?php echo $oneData->id ?>">

                            <label>Organization Name:</label><input class="form-control" name="name" value="<?php echo $oneData->name ?>"  type="text">
                            <br>
                            <label>Summary Of Organization</label>
                            <br>
                            <textarea id="summaryOfOrganization" name="summaryOfOrganization" rows="6" cols="30" value="<?php echo $oneData->summaryOfOrganization ?>" ></textarea>
                            <br>
                            <br>
                            <input class="btn btn-lg btn-success " type="submit" value="Update">
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>

</body>

</html>