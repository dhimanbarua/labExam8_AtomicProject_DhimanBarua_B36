<?php
namespace App\Gender;
use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;
use PDO;
class Gender extends DB
{
    public $id="";

    public $gender="";

    public $name="";

    public function __construct()
    {
        parent::__construct();
    }

    public function setData($data=NULL)
    {
        if(array_key_exists('id',$data))
        {
            $this->id = $data['id'];
        }
        if(array_key_exists('name',$data))
        {
            $this->name= $data['name'];
        }
        if(array_key_exists('gender',$data))
        {
            $this->gender = $data['gender'];
        }
    }
    public function store()
    {
        $arrData = array($this->name,$this->gender);
        $conn=$this->DBH;
        $STH=$conn->prepare("INSERT INTO gender(name,gender) VALUES(?,?) ");
        $STH->execute($arrData);

        if($STH)
        {
            Message::message("<div id='msg'><h3 align='center'>[User Name: $this->name],[Gender:$this->gender]
                    <br>Data Has been Inserted Successfully!!!!!!</h3></div> ");
        }
        else
        {
            Message::message("<div id='msg'><h3 align='center'>[User Name: $this->name],[Gender:$this->gender]
                    <br>Data Has Not been Inserted Successfully!!!!!!</h3> </div>");
        }
        Utility::redirect('create.php');

    }
    public function index(){

        $STH = $this->DBH->query("SELECT * FROM gender  WHERE is_deleted='No' ORDER BY gender DESC");

        $STH->setFetchMode(PDO::FETCH_OBJ);


        $arrAllData  = $STH->fetchAll();
        return $arrAllData;

    }// end of index();

    public function view(){

        $sql = 'SELECT * FROM gender WHERE id='.$this->id;

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);


        $arrOneData  = $STH->fetch();
        return $arrOneData;

    }// end of view();

    public function update(){

        $arrData = array ($this->name, $this->gender);
        $sql = "UPDATE gender SET name = ?, gender = ? WHERE id =".$this->id;

        $STH = $this->DBH->prepare($sql);
        $STH->execute($arrData);

        Utility::redirect('index.php');

    }// end of update()
    public function delete(){
        $sql = "DELETE FROM gender WHERE id =".$this->id;

        $STH = $this->DBH->prepare($sql);

        $STH->execute();
        Utility::redirect('index.php');
    }// end of delete
    public function trash(){
        $sql = "UPDATE gender SET is_deleted=NOW() WHERE id=".$this->id;

        $STH = $this->DBH->prepare($sql);
        $STH->execute();

        Utility::redirect('index.php');
    }// end of trash



}