<?php
namespace App\Birthday;
use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;
use PDO;
class Birthday extends DB
{
    public $id="";

    public $name="";

    public $birthday="";

    public function __construct()
    {
        parent::__construct();

    }
    public function setData($data=NULL)
    {
        if(array_key_exists('id',$data))
        {
            $this->id = $data['id'];
        }
        if(array_key_exists('name',$data))
        {
            $this->name = $data['name'];
        }
        if(array_key_exists('birthday',$data))
        {
            $this->birthday = $data['birthday'];
        }
    }
    public function store()
    {
        $arrData = array($this->name,$this->birthday);
        $conn=$this->DBH;
        $STH=$conn->prepare("INSERT INTO birthday(name,birthday) VALUES(?,?) ");
        $STH->execute($arrData);

        if($STH)
        {
            Message::message("<div id='msg'></div><h3 align='center'>[User Name: $this->name],[Birth Day:$this->birthday]
                    <br>Data Has been Inserted Successfully!!!!!!</h3> ");
        }
        else
        {
            Message::message("<div id='msg'></div><h3 align='center'>[User Name: $this->name],[Birth Day:$this->birthday]
                    <br>Data Has Not been Inserted Successfully!!!!!!</h3> ");
        }
        Utility::redirect('index.php');

    }

    public function index(){
        $STH = $this->DBH->query("SELECT * FROM birthday WHERE is_deleted='No' ORDER BY birthday DESC");

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $arrAllData = $STH->fetchAll();

        return $arrAllData;
    }

    public function view(){
        $sql = 'SELECT * FROM birthday WHERE id='.$this->id;

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrOneData = $STH->fetch();
        return $arrOneData;
    }

    public function update(){
        $arrData = array($this->name, $this->birthday);
        $sql = "UPDATE birthday SET name = ?, birthday = ? WHERE id=".$this->id;

        $STH = $this->DBH->prepare($sql);
        $STH->execute($arrData);
        Utility::redirect('index.php');
    }
    public function delete(){
        $sql = "DELETE FROM birthday WHERE id =".$this->id;

        $STH = $this->DBH->prepare($sql);

        $STH->execute();
        Utility::redirect('index.php');
    }// end of delete
    public function trash(){
        $sql = "UPDATE birthday SET is_deleted=NOW() WHERE id=".$this->id;

        $STH = $this->DBH->prepare($sql);
        $STH->execute();

        Utility::redirect('index.php');
    }// end of trash





}