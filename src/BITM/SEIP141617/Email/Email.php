<?php
namespace App\Email;
use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;
use PDO;
class Email extends DB
{
    public $id="";

    public $email="";

    public $name="";

    public function __construct()
    {
        parent::__construct();

    }

    public function setData($data=NULL)
    {
        if(array_key_exists('id',$data))
        {
            $this->id = $data['id'];
        }
        if(array_key_exists('name',$data))
        {
            $this->name= $data['name'];
        }
        if(array_key_exists('email',$data))
        {
            $this->email = $data['email'];
        }
    }
    public function store()
    {
        $arrData = array($this->name,$this->email);
        $conn=$this->DBH;
        $STH=$conn->prepare("INSERT INTO email(name,email) VALUES(?,?) ");
        $STH->execute($arrData);

        if($STH)
        {
            Message::message("<div id='msg'><h3 align='center'>[User Name: $this->name],[Email:$this->email]
                    <br>Data Has been Inserted Successfully!!!!!!</h3></div> ");
        }
        else
        {
            Message::message("<div id='msg'><h3 align='center'>[User Name: $this->name],[Email:$this->email]
                    <br>Data Has Not been Inserted Successfully!!!!!!</h3></div> ");
        }
        Utility::redirect('create.php');
    }

    public function index(){
        $STH = $this->DBH->query("SELECT * FROM email WHERE is_deleted='No' ORDER BY email DESC");

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $arrAllData = $STH->fetchAll();

        return $arrAllData;
    }

    public function view(){
        $sql = 'SELECT * FROM email WHERE id='.$this->id;

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrOneData = $STH->fetch();

        return $arrOneData;
    }
    public function update(){

        $arrData = array($this->name, $this->email);
        $sql = "UPDATE email SET name = ?, email = ? WHERE id =".$this->id;

        $STH = $this->DBH->prepare($sql);
        $STH->execute($arrData);

        Utility::redirect('index.php');

    }// end of update()
    public function delete(){
        $sql = "DELETE FROM email WHERE id =".$this->id;

        $STH = $this->DBH->prepare($sql);

        $STH->execute();
        Utility::redirect('index.php');
    }// end of delete
    public function trash(){
        $sql = "UPDATE email SET is_deleted=NOW() WHERE id=".$this->id;

        $STH = $this->DBH->prepare($sql);
        $STH->execute();

        Utility::redirect('index.php');
    }// end of trash



}